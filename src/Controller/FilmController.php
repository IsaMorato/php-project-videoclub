<?php

namespace App\Controller;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Films;
use App\Form\FilmsFormType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FilmController extends AbstractController{

    /**
     * @Route("/films",name="getFilms")
     */

     public function getAllFilms (EntityManagerInterface $doctrine){
         $repo = $doctrine->getRepository(Films::class);
         $films= $repo->findAll();
         return $this->render("film/film.html.twig",["films"=>$films]);
         
     }

     /**
      * @Route("films/create", name="postNewFilm")
      */
      public function postNewFilm (Request $req , EntityManagerInterface $doctrine){

      $form=$this->createForm(FilmsFormType::class);
        $form->handleRequest($req);
        if($form->isSubmitted() && $form->isValid()){
            $film= $form->getData();
            $user= $this->getUser();
            $film->setIdUser($user);
            $doctrine->persist($film);
            $doctrine->flush();
            return $this->redirectToRoute("getFilms");

        }
        return $this->render("film/formNewFilm.html.twig", ['filmsForm' =>$form->createView()]);
    }
    /**
     * @Route("/films/details/{id}", name='detailsFilm')
     */
/* 
     public function detailsFilm(Films $film ){
        dump($film);
       
       
        return $this->render("film/filmDetails.html.twig", ["film" => $film]); 
     } */
}